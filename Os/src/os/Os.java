package os;

import java.util.*;
import java.util.concurrent.SynchronousQueue;
import sos.*;

//Ignore my comments
//I was trying to set up a Job Table


public class Os{
	//static final int MAX_JOB_TABLE_SIZE = 50;  //It said in one of the papers to keep no more the 50 jobs in the job table
	static List<Job> jobTable;
	static Queue<Job> IOQueue;
	static MemoryManager memory;
	static int counter = 0;
	static int runningJob;
	static int jobCurrentlyMoving; //index of job moving from memory or whatever
	static int jobDoingIO;
	static Boolean isOnTrace;
	static Boolean drumBeingUsed;


	public static void startup(){
		// sos.offtrace();
		isOnTrace = false;
		jobTable = new ArrayList<Job>();
		IOQueue = new SynchronousQueue<Job>();
		memory = new MemoryManager();
		//set isOnTrace to true when offtrace() is called
	}

	public static void Crint(int [] a, int [] p){ 
		bookKeeper(p[5]);
		jobTable.add(new Job(p[1], p[2], p[3], p[4], p[5]));

		Swapper(jobTable.get(jobTable.size() - 1), 0);	//0 is from drum to memory, 1 is from memoy back to drum
		runningJob = CPUScheduler.scheduler(memory);
		runJob(a, p);

	}
	public static void Dskint (int []a, int []p){
		
		swapper(jobTable.get(nonInMemory()));
		runningJob = CPUScheduler.scheduler(memory);
		runJob(a, p);
	}

	public static void Drmint (int []a, int []p){
		drumBeingUsed = false;
		bookKeeper(p[5]);
		
		jobTable.get(jobCurrentlyMoving).setInMemory(true);
		int tempJobIndex = nonInMemory();
		if (tempJobIndex >= 0)	
			swapper(jobTable.get(tempJobIndex), 0);
		
		swapper(jobTable.get(nonInMemory()));
		runningJob = CPUScheduler.scheduler(memory);
		runJob(a, p);

	}

	public static void Tro (int []a, int []p){
		
		swapper(jobTable.get(nonInMemory()));
		runningJob = CPUScheduler.scheduler(memory);
		runJob(a, p);
	}

	public static void Svc (int []a, int []p){
		bookKeeper(p[5]);
		if(a = 5){
			if (jobTable.get(runningJob).getIoRequests == 0 && jobTable.get(runningJob).getIsKilled){
				memory.remove(jobTable.get(runningJob).getLocation);
				jobTable.get(runningJob).setInMemory(false);
				//updateJobTable should take care of the deleting out of the table
			}
			else
				jobTable.get(runningJob).setIsKilled(true);
		}
		else if(a = 6){
			
		}
		else if(a = 7){
			
		}
		swapper(jobTable.get(nonInMemory()));
		runningJob = CPUScheduler.scheduler(memory);
		runJob(a, p);
		
	}


	public static void Swapper(Job j, int inOrOut){
		if(!drumBeingUsed){
			updateJobTable();
			if(j.getJobSize() < memory.freeSpace()){
				j.setLocation(memory.spotInMemory());
				jobCurrentlyMoving = j.getJobNumber();
				sos.siodrum(j.getJobNum(), j.getJobSize(), j.getLocation(), inOrOut);
				memory.add(j);
				drumBeingUsed = true;
			}
			else
				sos.siodrum((j.getJobNum(), j.getJobSize(), j.getLocation(), 1); //put it back in drum

		}
	}

	public static void bookKeeper(int time){
		int temp = time - jobTable.get(runningJob).getEntryTime();
		jobTable.get(runningJob).setCurrentTime(jobTable.get(runningJob).getMaxCPUTime();
		jobTable.get(runningJob).setMaxCPUTime(jobTable.get(runningJob).getMaxCPUTime() - temp);
	}

	public static void runJob(int [] a, int [] p){
		if(jobTable.get(runningJob).getInMemory && !jobTable.get(runingJob).getBlocked()){
			a = 1;
		}
		else{
			a = 2;
			p[2] = jobTable.get(runningJob).getLocation();
			p[3] = jobTable.get(runningJob).getJobSize();
			p[4] = jobTable.get(runningJob).getMaxCPUTime();
			jobTable.get(runningJob).setEntryTime(p[5]);
			jobTable.get(runningJob).setInCpu(true);
		}
	}

	public static void updateJobTable(){
		for(int i = 0; i < jobTable.size(); i++){
			if (!jobTable.get(i).getInMemory)
				jobTable.remove(i);
			if (jobTable.get(i).getIsKilled && jobTable.get(i).getIoRequests == 0){
				jobTable.remove(i);
			}
		}
	}
	
	public static int notInMemory(){
		for (int i = 0; i < jobTable.size(); i++){
			if(!jobTable.get(i).getInMemory){
				return i;
			}
		}
		return -1;
	}




}
